import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		Main command = new Main();
		ArrayList<Dog> dogs = new ArrayList<Dog>();

		command.fillUpDog(dogs);
		command.showDog(dogs);

	}

	public void fillUpDog(ArrayList<Dog> dogs){

		for(int i = 0; i<5;i++){
			Dog dog = new Dog("Name"+i);
			dogs.add(dog);
		}

	}

	public void showDog(ArrayList<Dog> dogs){
		for(int i = 0; i<5;i++){
			System.out.println(dogs.get(i).nickName + " run = "+dogs.get(i).run());
		}
	}

}




public class Dog {

	public String nickName;

	Dog(String name){
		this.nickName=name;
	}

	public int run(){
		return (int)(Math.random()*20 +10);
	}

}
